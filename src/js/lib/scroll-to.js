
function smoothScroll() {

    let links = $('a[href*="#"]:not([href="#"])');

    function scrollTo(id) {
        let el = $(id);
        
        if (!el.length) {
            return;
        } 

        $('html, body').animate({
            scrollTop: el.offset().top - 100
        }, 500);
    }

    links.on('click', function (e) {
        e.preventDefault();
        scrollTo($(this).attr('href'))
    });

}
module.exports = smoothScroll;