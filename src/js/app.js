import smoothScroll from './lib/scroll-to';


let lastScrollTop = 0;
$(window).scroll(function () {
   const st = $(this).scrollTop();
   const $header = $('.layout__header');
 
   if($header){
       if($(window).scrollTop() > 10){
           $('.layout__header').addClass('is-fixed');
       } else {
           $('.layout__header').removeClass('is-fixed');
       }
        if(!$(window).width() > 1024){
            if (st > lastScrollTop){   
                $('.layout__header').removeClass('is-fixed');
            } else if(st <= lastScrollTop) {
                if($(window).scrollTop() > 100){
                    $('.layout__header').addClass('is-fixed');
                } else {
                    $('.layout__header').removeClass('is-fixed');
                }
            } 
            lastScrollTop = st;
        }
   } 

});



function sortTableRes(){
    const $target = $('#sortTable');
    const names = [];
    const trVal = [];
    $('#sortTable input:checked').each(function(i,el){
        names.push($(el).attr('name'));
        trVal.push($(el).val());
    });
    // console.log(names);

    $('[data-filter_week]', $target).hide().filter($(`[data-filter_${names[0]}="${trVal[0]}"`)).filter($(`[data-filter_${names[1]}="${trVal[1]}"`)).show();
}
$('#sortTable input').each(function(){
    $(this).on('input', function(){
        sortTableRes();
    })
})


// check - visible elems with targer btn or not
function isScrolledIntoView(elem){
    const $elems = elem;
    const docViewTop = $(window).scrollTop();
    const docViewBottom = docViewTop + $(window).height();
    const elemTop = $elems.offset().top;
    const elemBottom = elemTop + $elems.height();
    if((elemBottom <= docViewBottom) && (elemTop >= docViewTop)){
        return(1);
    } 
        return(0);
    
}
function arraySum(array){
    let sum = 0;
    for(let i = 0; i < array.length; i++){
        sum += array[i];
        }
    // console.log(sum);
    if(sum >= 1){
        $('.helper-sticky__wrapper').addClass('is-hidden');
    } else{
        $('.helper-sticky__wrapper').removeClass('is-hidden');
    }
}
function changeClass(){
    const $elems = $('.js-helper-hidden');
    const arr = [];
    $elems.each(function(){
        const $el = $(this);
        // console.log(isScrolledIntoView(_that));
        arr.push(isScrolledIntoView($el));
    })
    arraySum(arr);

    // console.log(arr);
    
}
changeClass();


function accordion(parent, val, elem){
    if(parent.length > 0){
        // console.log(elem);
        const $elemParent = $(elem).closest('.accordion__item');


        if(elem !== undefined && $elemParent.is('.active')){
            $('.accordion__body', $elemParent).slideUp('200');
            $elemParent.removeClass('active');

        } else {
            $(`.accordion__item:not(:nth-child(${val+1})) .accordion__body`).slideUp('200').closest('.accordion__item').removeClass('active');
            $(`.accordion__item:nth-child(${val+1}) .accordion__body`).slideDown('200').closest('.accordion__item').addClass('active');
        }


        // if(){
        //     $(`.accordion__item:nth-child(${val+1}) .accordion__body`).slideUp('200').closest('.accordion__item').removeClass('active');
        // } else {

            // $(`.accordion__item:not(:nth-child(${val+1})) .accordion__body`).slideUp('200').closest('.accordion__item').removeClass('active');
            // $(`.accordion__item:nth-child(${val+1}) .accordion__body`).slideDown('200').closest('.accordion__item').addClass('active');
        // }
        
    }
}
$('.accordion__head').on('click', function(){
    const $parent = $($(this).closest('.accordion'));
    const index = $(this).closest('.accordion__item').index();

    // console.log($parent);
    // console.log(index);
    accordion($parent ,index, $(this));
    
})


$(document).on('click', '.menu-trigger', function(){
    $('body').toggleClass('menu-open');
});

$(document).ready(function(){
    $('.lazy-slider-wrapper .swiper-button_prev').on('click', function() {
        const $slide = $('.lazy-slider .slide:first-child');
        
        $('.lazy-slider').append($slide.clone().css('display', 'none').fadeIn(200));
        $slide.fadeOut();
        $slide.remove();
    })
    $('.lazy-slider-wrapper .swiper-button_next').on('click', function() {
        const $slide = $('.lazy-slider .slide:last-child');
        
        $('.lazy-slider').prepend($slide.clone());
        $slide.fadeOut();
        $slide.remove();
    })
    sortTableRes();
    accordion($('#accordion1'),0);
    smoothScroll();
    
});

$(window).on('scroll', function() {
    changeClass();

});