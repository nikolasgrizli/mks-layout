<?php

/*
  Template Name: Versus
  Template Post Type: post, page

 */

remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_excerpt', 'wpautop' );

add_filter( 'body_class', '__return_false' );
add_filter( 'nav_menu_css_class', '__return_false' );

add_filter( 'fl_topbar_enabled', '__return_false' );
add_filter( 'fl_header_enabled', '__return_false' );

add_filter( 'fl_footer_enabled', '__return_false' );

get_header('review');

?>

<?php
?>

<?php
if (have_posts()) :
    while (have_posts()) :
        the_post();
        get_template_part('content', 'versus');
    endwhile;
endif;
?>

<?php get_footer('review'); ?>
